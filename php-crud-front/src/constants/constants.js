import Vue from 'vue'

Vue.mixin({
    beforeCreate() {
        const options = this.$options;
        if ( options.constants ) {
            this.$constants = options.constants;
        } else if ( options.parent && options.parent.$constants ) {
            this.$constants = options.parent.$constants;
        }
    }
});

export default {
    API_HOST: 'http://php-crud.desenvolvimento/api/',
}