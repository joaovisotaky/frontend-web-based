import RestService from '../services/RestService'

export default class PessoaProvider {
    http = null
    constructor(url) {
        this.http = new RestService(url)
    }

    buscarPessoas() {
        return this.http.get('/pessoa').then(response => response.data)
    }

    salvarPessoa(dados) {
        return this.http.post('/pessoa', dados).then(response => response.data)
    }

    excluirPessoa(idPessoa) {
        return this.http.delete(`/pessoa/${idPessoa}`).then(response => response.data)
    }

    buscarPessoa(idPessoa) {
        return this.http.get(`/pessoa/${idPessoa}`).then(response => response.data)
    }

    buscarDadosGraficoIdade() {
        return this.http.get('/pessoa/idade').then(response => response.data)
    }

    buscarDadosGraficoGenero() {
        return this.http.get('/pessoa/genero').then(response => response.data)
    }
}