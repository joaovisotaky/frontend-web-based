import RestService from '../services/RestService'

export default class LoginProvider {
    http = null
    constructor(url) {
        this.http = new RestService(url, false);
    }

    efetuarLogin(dados) {
        return this.http.post('/login', dados).then(response => response.data);
    }
}