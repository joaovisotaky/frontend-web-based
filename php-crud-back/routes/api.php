<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'LoginController@autenticar');

Route::group(['middleware' => 'jwt.auth'], function() {
    Route::post('/pessoa', 'PessoaController@salvar');
    Route::delete('/pessoa/{id}', 'PessoaController@apagar');
    Route::get('/pessoa/idade', 'PessoaController@buscarDadosGraficoIdade');
    Route::get('/pessoa/genero', 'PessoaController@buscarDadosGraficoGenero');
    Route::get('/pessoa/{id}', 'PessoaController@buscar');
    Route::get('/pessoa', 'PessoaController@buscar');

    Route::get('/login/sair', 'LoginController@sair');
});
