<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class JsonValidator extends Facade {

    protected static function getFacadeAccessor() {
        return 'JsonValidator';
    }
}