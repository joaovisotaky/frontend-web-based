<?php

namespace App\Repositories\Contract;


interface IRepository
{
    public function findAll();

    public function find($id);

    public function create(array $attributes);

    public function update(array $attributes);

    public function delete($id);
}