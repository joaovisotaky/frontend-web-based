<?php

namespace App\Repositories;

use App\Facades\JsonValidator;
use App\Repositories\Contract\IRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

abstract class Repository implements IRepository {
    private $app;
    protected $model;
    protected $primaryKey;

    function __construct(App $app) {
        $this->app = $app;
        $this->model = $this->createModel();
        $this->primaryKey = $this->getPrimaryKeyName();
        $idPessoaLogada = (Auth::user())->id_pessoa;
        DB::statement(DB::raw("SET @USER = {$idPessoaLogada};"));

    }

    public abstract function getModel();

    public function createModel() {
        $modelName = $this->getModel();
        $model = $this->app->make($modelName);

        if (!$model instanceof Model) {
            throw new Exception('Não foi possível instanciar o model: ' . $modelName);
        }

        return $model->newQuery();
    }

    public abstract function getPrimaryKeyName();

    public function findAll(array $columns = ['*']) {
        return $this->model->get($columns);
    }

    public function find($id, array $columns = ['*']) {
        return $this->model->find($id, $columns);
    }

    public function create(array $attributes) {
        try {
            DB::beginTransaction();
            $attributes = $this->transform($attributes);
            $this->validate($attributes);
            $this->model->create($attributes);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function update(array $attributes) {
        try {
            DB::beginTransaction();
            $this->validate($attributes);
            $this->model->find($attributes[$this->getPrimaryKeyName()])->fill($this->transform($attributes))->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function delete($id) {
        try {
            DB::beginTransaction();
            $this->model->find($id)->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    private function validate(array $data) {
        JsonValidator::validate($data, $this->getValidationRules($data));
    }

    public abstract function getValidationRules(array $data);

    public function transform(array $attributes) {
        return $attributes;
    }
}