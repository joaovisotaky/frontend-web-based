<?php

namespace App\Http\Controllers;

use App\Repositories\PessoaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;
use \DateTime;

class PessoaController extends Controller {
    public function buscar($idPessoa = null, PessoaRepository $pessoaRepository) {
        if (empty($idPessoa)) {
            $pessoa = $pessoaRepository->findAll([
                'id_pessoa',
                'nome',
                'email',
                'rg',
                'data_nascimento',
                'sexo',
                'endereco'
            ])->toArray();

            return response()->json($pessoa);
        }

        $pessoa = collect($pessoaRepository->find($idPessoa))->except([
            'senha'
        ])->toArray();

        return response()->json($pessoa);
    }

    public function salvar(Request $request, PessoaRepository $pessoaRepository) {
        $pessoa = $request->all();

        if (!empty($pessoa['id_pessoa'])) {
            return $pessoaRepository->update($pessoa);
        }

        return $pessoaRepository->create($pessoa);
    }

    public function apagar($idPessoa, PessoaRepository $pessoaRepository) {
        if (empty($idPessoa)) {
            throw new InvalidArgumentException('O id da pessoa é obrigatório para exclusão');
        }

        return $pessoaRepository->delete($idPessoa);
    }

    public function buscarDadosGraficoIdade(PessoaRepository $pessoaRepository) {
        $pessoas = $pessoaRepository->findAll();
        $faixas = [10 => "0 a 9", 20 => "10 a 19", 30 => "20 a 29", 40 => "30 a 39", 50 => "Maior que 40"];
        $pessoasPorFaixa = [];

        $dataAtual = new DateTime();

        foreach ($faixas as $faixa => $nomeFaixa) {
            $pessoasPorFaixa[$nomeFaixa] = 0;
            foreach ($pessoas as $i => $pessoa) {
                $dataNascimento = new DateTime($pessoa['data_nascimento']);

                $interval = $dataAtual->diff($dataNascimento);
                $idade = $interval->format('%Y');

                if ($idade < $faixa) {
                    $pessoasPorFaixa[$nomeFaixa]++;
                    unset($pessoas[$i]);
                }
            }
        }

        return [
            'labels' => array_keys($pessoasPorFaixa),
            'values' => array_values($pessoasPorFaixa)
        ];

    }

    public function buscarDadosGraficoGenero(PessoaRepository $pessoaRepository) {
        $pessoas = $pessoaRepository->findAll()->groupBy('sexo')->toArray();
        $pessoasPorGenero = [
            'Masculino' => count($pessoas['M']),
            'Feminino' => count($pessoas['F']),
        ];

        return [
            'labels' => array_keys($pessoasPorGenero),
            'values' => array_values($pessoasPorGenero)
        ];

    }
}
