<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Pessoa extends Authenticatable
{
    protected $table = 'pessoa';
    protected $primaryKey = 'id_pessoa';
    public $timestamps = false;

    protected $fillable = [
        'email', 'senha', 'nome', 'rg', 'data_nascimento', 'sexo', 'endereco'
    ];

    protected $hidden = [
        'password',
    ];

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->senha;
    }
}
